package com.povio.cryptoprices.ui.fragment.settings;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.NumberPicker;
import android.widget.Spinner;

import com.povio.cryptoprices.R;
import com.povio.cryptoprices.util.InjectorHelper;
import com.povio.cryptoprices.util.Logger;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */
public class FragmentCryptoSettings extends DialogFragment implements CryptoSettingsView {

    @BindView(R.id.spinner_currency)
    Spinner spinner_currency;

    @BindView(R.id.currency_number_picker)
    NumberPicker currency_number_picker;

    @Inject
    CryptoSettingsPresenter presenter;

    @Inject
    ArrayAdapter adapterCurrencies;

    private Unbinder unbinder;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = null;
        try {
            rootView = (ViewGroup) inflater.inflate(R.layout.fragment_crypto_settings_layout, container, false);
            unbinder = ButterKnife.bind(this, rootView);
            InjectorHelper.inject(this);

            adapterCurrencies.setDropDownViewResource(R.layout.spinner_currency_item_layout);
            spinner_currency.setAdapter(adapterCurrencies);

            presenter.setView();

        } catch (Throwable e) {
            e.printStackTrace();
            Logger.e(FragmentCryptoSettings.class, "Error", e);
        }

        return rootView;
    }

    @Override
    public void initCurrencyNumberLimit(int min, int max) {
        currency_number_picker.setMinValue(min);
        currency_number_picker.setMaxValue(max);
    }

    @Override
    public void setCurrency(String currency) {
        spinner_currency.setSelection(adapterCurrencies.getPosition(new SpinnerValue<>(currency)));
    }

    @Override
    public String getCurrency() {
        SpinnerValue<String> val = (SpinnerValue) spinner_currency.getSelectedItem();
        return val.getValue();
    }

    @Override
    public void setCurrencyLimit(int limit) {
        currency_number_picker.setValue(limit);
    }

    @Override
    public int getCurrencyLimit() {
        return currency_number_picker.getValue();
    }

    @Override
    public void close() {
        dismiss();
    }

    @OnClick(R.id.settings_button_confirm)
    public void onConfirmButtonClick() {
        presenter.save();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
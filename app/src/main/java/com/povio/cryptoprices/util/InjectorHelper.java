package com.povio.cryptoprices.util;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.povio.cryptoprices.dialog.di.ModuleDialogManager;
import com.povio.cryptoprices.errorhandler.di.ModuleErrorHandler;
import com.povio.cryptoprices.navigation.di.ModuleNavigationController;
import com.povio.cryptoprices.ui.activity.CryptoListActivity;
import com.povio.cryptoprices.ui.activity.di.ComponentActivity;
import com.povio.cryptoprices.ui.fragment.details.FragmentCryptoCurrencyDetails;
import com.povio.cryptoprices.ui.fragment.details.di.ModuleCryptoCurrencyDetails;
import com.povio.cryptoprices.ui.fragment.list.FragmentCryptoCurrencyList;
import com.povio.cryptoprices.ui.fragment.list.di.ModuleCryptoCurrencyList;
import com.povio.cryptoprices.ui.fragment.list.dto.CryptoElement;
import com.povio.cryptoprices.ui.fragment.settings.FragmentCryptoSettings;
import com.povio.cryptoprices.ui.fragment.settings.ModuleCryptoSettings;

import static com.povio.cryptoprices.ui.fragment.details.Constants.DETAILS_BUNDLE_PARAM;

public class InjectorHelper {

    public static ComponentActivity inject(CryptoListActivity activity, @IdRes int layoutId, @NonNull FragmentManager fragmentManager) {
        return DIUtils.getComponentApp(activity)
                .get(new ModuleErrorHandler(activity), new ModuleNavigationController(layoutId, fragmentManager), new ModuleDialogManager(activity));
    }

    public static void inject(CryptoListActivity activity) {
        DIUtils.getComponentActivity(activity)
                .get()
                .inject(activity);
    }

    public static void inject(FragmentCryptoCurrencyList fragment) {
        DIUtils.getComponentActivity(fragment)
                .get(new ModuleCryptoCurrencyList(fragment))
                .inject(fragment);
    }

    public static void inject(FragmentCryptoSettings fragment) {
        DIUtils.getComponentActivity(fragment)
                .get(new ModuleCryptoSettings(fragment, fragment.getActivity().getApplicationContext()))
                .inject(fragment);
    }

    public static void inject(FragmentCryptoCurrencyDetails fragment) {
        Bundle details = fragment.getArguments();
        CryptoElement cryptoCurrencyDetails = details.getParcelable(DETAILS_BUNDLE_PARAM);
        DIUtils.getComponentActivity(fragment)
                .get(new ModuleCryptoCurrencyDetails(fragment, cryptoCurrencyDetails))
                .inject(fragment);
    }
}

package com.povio.cryptoprices.preference;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.ListIterator;

import static com.povio.cryptoprices.preference.Constantns.DEFAULT_CURRENCY_NUMBER_LIMIT;
import static com.povio.cryptoprices.preference.Constantns.DEFAULT_FIAT_CURRENCY;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/17/18.
 */

public class CryptoPreferenceImpl implements CryptoPreference {

    private final Application app;
    private ArrayList<PrefsPropertyListener> listeners = new ArrayList<>();

    public CryptoPreferenceImpl(Application app) {
        this.app = app;
    }

    @Override
    public String getFiatCurrency() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(app);
        return settings.getString(PROPERTY_FIAT_CURRENCY, DEFAULT_FIAT_CURRENCY);
    }

    @Override
    public void setFiatCurrency(String fiatCurrency, boolean notify) {

        String oldValue = getFiatCurrency();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(app);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PROPERTY_FIAT_CURRENCY, fiatCurrency);
        editor.commit();

        if (notify) notifyPrefsPropertyListener(PROPERTY_FIAT_CURRENCY, oldValue, fiatCurrency);
    }

    @Override
    public int getCurrencyNumberLimit() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(app);
        return settings.getInt(PROPERTY_CURRENCY_NUMBER_LIMIT, DEFAULT_CURRENCY_NUMBER_LIMIT);
    }

    @Override
    public void setCurrencyNumberLimit(int limit, boolean notify) {

        String oldValue = getFiatCurrency();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(app);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(PROPERTY_CURRENCY_NUMBER_LIMIT, limit);
        editor.commit();

        if (notify) notifyPrefsPropertyListener(PROPERTY_CURRENCY_NUMBER_LIMIT, oldValue, limit);
    }

    @Override
    public void addPrefsPropertyListener(PrefsPropertyListener listener) {
        ListIterator<PrefsPropertyListener> it = listeners.listIterator();
        while (it.hasNext()) {
            PrefsPropertyListener wr = it.next();
            if (wr == listener) {
                return;
            }
        }
        listeners.add(listener);
    }

    @Override
    public void removePrefsPropertyListener(PrefsPropertyListener listener) {
        ListIterator<PrefsPropertyListener> it = listeners.listIterator();
        while (it.hasNext()) {
            PrefsPropertyListener wr = it.next();
            if (wr == listener) {
                it.remove();
            }
        }
    }

    /**
     * Call this "manually" after any of the properties have changed in order to trigger subscribed listeners.
     */
    @Override
    public void notifyPrefsListener() {
        notifyPrefsPropertyListener(PROPERTY_ANY, null, null);
    }

    private void notifyPrefsPropertyListener(String propertyName, Object oldValue, Object newValue) {
        notifyPrefsPropertyListener(new PrefsPropertyChangeEvent(this, propertyName, oldValue, newValue));
    }

    private void notifyPrefsPropertyListener(PrefsPropertyChangeEvent event) {
        for (PrefsPropertyListener wr : listeners) {
            wr.propertyChange(event);
        }
    }
}

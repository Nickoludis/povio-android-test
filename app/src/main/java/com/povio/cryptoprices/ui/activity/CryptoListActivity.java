package com.povio.cryptoprices.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.povio.cryptoprices.ComponentProvider;
import com.povio.cryptoprices.R;
import com.povio.cryptoprices.animation.AnimationUtils;
import com.povio.cryptoprices.dialog.DialogManager;
import com.povio.cryptoprices.navigation.NavigationController;
import com.povio.cryptoprices.ui.activity.di.ComponentActivity;
import com.povio.cryptoprices.ui.fragment.list.FragmentCryptoCurrencyList;
import com.povio.cryptoprices.ui.fragment.settings.FragmentCryptoSettings;
import com.povio.cryptoprices.util.InjectorHelper;

import javax.inject.Inject;

public class CryptoListActivity extends AppCompatActivity implements ComponentProvider {

    private ComponentActivity componentActivity;

    @Inject
    NavigationController navigationController;

    @Inject
    DialogManager dialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        componentActivity = InjectorHelper.inject(this, R.id.main_fragment_container, getSupportFragmentManager());
        InjectorHelper.inject(this);

        setContentView(R.layout.activity_crypto_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        navigateToStartFragment();
    }

    public void navigateToStartFragment() {
        navigationController.loadPage(FragmentCryptoCurrencyList.class)
                .addToBackStack(true)
                .isDialog(true)
                .animation(AnimationUtils.Transition.FADE)
                .load();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_crypto_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_settings:

                navigationController.loadPage(FragmentCryptoSettings.class)
                        .addToBackStack(true)
                        .isDialog(true)
                        .animation(AnimationUtils.Transition.FADE)
                        .load();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (navigationController.canGoBack()) {
            if (getSupportFragmentManager().getFragments().size() == 1) {
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public <T> T component(Class<T> type) {
        if (ComponentActivity.class == type) {
            return (T) componentActivity;
        }
        ComponentProvider componentProvider = ((ComponentProvider) getApplication());
        return componentProvider.component(type);
    }
}

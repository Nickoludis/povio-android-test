package com.povio.cryptoprices.ui.fragment.list.api;

import com.povio.cryptoprices.ui.fragment.list.dto.DTOCryptoElement;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface APICryptoData {

    @GET("ticker/")
    Observable<List<DTOCryptoElement>> getCryptoList(@Query("convert") String fiatCurrency, @Query("limit") String limit);

    @GET("ticker/{id}/")
    Observable<List<DTOCryptoElement>> getCryptoDetails(@Path("id") String cryptoCurrencyId, @Query("convert") String fiatCurrency);
}
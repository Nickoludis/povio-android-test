package com.povio.cryptoprices.ui.fragment.details;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.povio.cryptoprices.R;
import com.povio.cryptoprices.util.Logger;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/15/18.
 */

public class TestFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.test_fragment_layout, container, false);

        Logger.d(TestFragment.class, "onCreateView: " + this);

        return view;
    }

}

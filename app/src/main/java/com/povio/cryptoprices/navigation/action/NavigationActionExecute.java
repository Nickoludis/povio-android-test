package com.povio.cryptoprices.navigation.action;

/**
 * Represents parameter in fragment transaction operation.
 */
public interface NavigationActionExecute {

    /**
     * Performs fragment transaction navigation.
     */
    void load();
}

package com.povio.cryptoprices.ui.fragment.list;

import com.povio.cryptoprices.errorhandler.ErrorHandler;
import com.povio.cryptoprices.preference.CryptoPreference;
import com.povio.cryptoprices.ui.fragment.list.api.APICryptoData;
import com.povio.cryptoprices.ui.fragment.list.dto.CryptoElement;
import com.povio.cryptoprices.util.ResourceGetter;
import com.povio.cryptoprices.util.rxutils.DefaultSubscriber;
import com.povio.cryptoprices.util.rxutils.MSchedulers;
import com.povio.cryptoprices.util.rxutils.Task;
import com.povio.cryptoprices.util.rxutils.TaskImp;

import rx.Observable;
import rx.Subscription;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */
public class CryptoCurrencyListInteractorImpl implements CryptoCurrencyListInteractor {

    private ErrorHandler errorHandler;
    private final APICryptoData api;
    private final MSchedulers schedulers;
    private final ResourceGetter resourceGetter;
    private final CryptoPreference preference;

    public CryptoCurrencyListInteractorImpl(ErrorHandler errorHandler, APICryptoData api, MSchedulers schedulers, ResourceGetter resourceGetter, CryptoPreference preference) {
        this.errorHandler = errorHandler;
        this.api = api;
        this.schedulers = schedulers;
        this.resourceGetter = resourceGetter;
        this.preference = preference;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Task getData(CryptoListListener listener, String fiatCurrency, String limit) {
        Subscription s = api.getCryptoList(fiatCurrency, limit)
                .subscribeOn(schedulers.newThread())
                .flatMap(dtoElements -> Observable.from(dtoElements))
                .map(dtoElement -> new CryptoElement(dtoElement, resourceGetter, preference.getFiatCurrency()))
                .toList()
                .observeOn(schedulers.mainThread())
                .subscribe(new DefaultSubscriber<>(listener, errorHandler));

        return new TaskImp(s);
    }
}
package com.povio.cryptoprices.ui.fragment.settings;

import com.povio.cryptoprices.annotation.PerActivity;

import dagger.Subcomponent;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */
@PerActivity
@Subcomponent(modules = {ModuleCryptoSettings.class})
public interface ComponentCryptoSettings {
    void inject(FragmentCryptoSettings fragment);
}
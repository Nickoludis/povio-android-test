package com.povio.cryptoprices.ui.fragment.settings;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/17/18.
 */
public class SpinnerValue<T> {
    private T value;
    private String viewRepresentation;

    public SpinnerValue(T value) {
        this.value = value;
        this.viewRepresentation = value.toString();
    }

    public SpinnerValue(T value, String viewRepresentation) {
        this.value = value;
        this.viewRepresentation = viewRepresentation;
    }

    public T getValue() {
        return value;
    }

    @Override
    public String toString() {
        return viewRepresentation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpinnerValue that = (SpinnerValue) o;

        return value.equals(that.value);

    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}

package com.povio.cryptoprices.navigation.action;

import com.povio.cryptoprices.navigation.NavigationActionBuilder;

/**
 * Represents parameter in fragment transaction operation.
 */
public interface NavigationActionIsDialog {

    /**
     * Receives boolean value which defines whether fragment is dialog or not.
     *
     * @param isDialog true | false
     * @return NavigationActionBuilder instance
     */
    NavigationActionBuilder isDialog(boolean isDialog);
}

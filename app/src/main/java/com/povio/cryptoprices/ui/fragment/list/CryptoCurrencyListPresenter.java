package com.povio.cryptoprices.ui.fragment.list;

import com.povio.cryptoprices.network.NNetworkInfo;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */
public interface CryptoCurrencyListPresenter {

    void loadData();

    void cancelLoading();

    void onNetworkChange(NNetworkInfo info);

    void onDestroy();
}
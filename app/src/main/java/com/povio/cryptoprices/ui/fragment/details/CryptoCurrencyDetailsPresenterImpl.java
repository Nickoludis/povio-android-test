package com.povio.cryptoprices.ui.fragment.details;

import com.povio.cryptoprices.network.NNetworkInfo;
import com.povio.cryptoprices.network.NetworkManager;
import com.povio.cryptoprices.network.NetworkStateReceiverListener;
import com.povio.cryptoprices.preference.CryptoPreference;
import com.povio.cryptoprices.preference.PrefsPropertyChangeEvent;
import com.povio.cryptoprices.preference.PrefsPropertyListener;
import com.povio.cryptoprices.ui.fragment.list.dto.CryptoElement;
import com.povio.cryptoprices.util.rxutils.Task;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/18/18.
 */
public class CryptoCurrencyDetailsPresenterImpl implements CryptoCurrencyDetailsPresenter, NetworkStateReceiverListener, PrefsPropertyListener {

    private final CryptoCurrencyDetailsView view;
    private CryptoCurrencyDetailsInteractor interactor;
    private final CryptoElement cryptoElement;

    private final NetworkManager networkManager;
    private final CryptoPreference preference;

    private Task loadTask;
    protected boolean networkStatusListenerSet = false;

    public CryptoCurrencyDetailsPresenterImpl(CryptoCurrencyDetailsView view, CryptoCurrencyDetailsInteractor interactor, CryptoElement cryptoElement, NetworkManager networkManager, CryptoPreference preference) {
        this.view = view;
        this.interactor = interactor;
        this.cryptoElement = cryptoElement;
        this.networkManager = networkManager;
        this.preference = preference;
    }

    @Override
    public void setData() {

        if (!networkStatusListenerSet) {
            networkManager.addListener(this);
            networkStatusListenerSet = true;
        }
        preference.addPrefsPropertyListener(this);
        populateData(cryptoElement);
    }

    @Override
    public void refreshData() {
        executeLoadJob();
    }

    @Override
    public void cancelLoading() {
        if (loadTask != null) loadTask.end();
    }

    @Override
    public void onNetworkChange(NNetworkInfo info) {
        if (info.isConnected()) {
            executeLoadJob();
        }
    }

    @Override
    public void onDestroy() {
        networkManager.removeListener(this);
        preference.removePrefsPropertyListener(this);
    }

    @Override
    public void onNetworkStateChange(NNetworkInfo info) {
        onNetworkChange(info);
    }

    @Override
    public void propertyChange(PrefsPropertyChangeEvent evt) {
        String propertyName = evt.getPropertyName();
        if (preference.PROPERTY_ANY.equals(propertyName)) {
            executeLoadJob();
        }
    }

    /////////////////////
    // PRIVATE METHODS //
    /////////////////////

    private void executeLoadJob() {

        if (loadTask == null || loadTask.isEnded()) {

            view.showLoadingAnimation(true);

            loadTask = interactor.getData(new CryptoCurrencyDetailsInteractor.CryptoDetailsListener() {
                @Override
                public boolean onError(Throwable error) {
                    view.showLoadingAnimation(false);
                    return false;
                }

                @Override
                public void onCompleted() {
                    view.showLoadingAnimation(false);
                }

                @Override
                public void onResult(CryptoElement cryptoElement) {
                    populateData(cryptoElement);
                }
            }, cryptoElement.id, preference.getFiatCurrency());
        }
    }

    private void populateData(CryptoElement cryptoElement) {
        view.setRank(cryptoElement.rank);
        view.setName(cryptoElement.name);
        view.setSymbol(cryptoElement.symbol);
        view.setPrice(cryptoElement.price);
        view.set24hVolume(cryptoElement.volume24h);
        view.setMarketCap(cryptoElement.marketCap);
        view.setPriceInBitcoin(cryptoElement.priceBitCoin);
        view.setChange1h(cryptoElement.change1h, cryptoElement.color1h);
        view.setChange24h(cryptoElement.change24h, cryptoElement.color24h);
        view.setChange7d(cryptoElement.change7d, cryptoElement.color7d);
        view.setTotalSupply(cryptoElement.totalSupply);
        view.setAvailableSupply(cryptoElement.availableSupply);
    }
}
package com.povio.cryptoprices;

import com.povio.cryptoprices.dialog.di.ModuleDialogManager;
import com.povio.cryptoprices.errorhandler.di.ModuleErrorHandler;
import com.povio.cryptoprices.navigation.di.ModuleNavigationController;
import com.povio.cryptoprices.network.di.ModuleNetworkManager;
import com.povio.cryptoprices.network.http.ModuleHTTPClient;
import com.povio.cryptoprices.preference.di.ModuleCryptoPreference;
import com.povio.cryptoprices.ui.activity.di.ComponentActivity;
import com.povio.cryptoprices.util.rxutils.ModuleScheduler;

import javax.inject.Singleton;

import dagger.Component;


/**
 * The component which lifetime is the life of the Application
 */

@Singleton
@Component(modules = {ModuleApp.class, ModuleNetworkManager.class, ModuleScheduler.class, ModuleHTTPClient.class, ModuleCryptoPreference.class})
public interface ComponentApp {

    ComponentActivity get(ModuleErrorHandler moduleErrorHandler, ModuleNavigationController moduleNavigationController, ModuleDialogManager moduleDialogManager);
}

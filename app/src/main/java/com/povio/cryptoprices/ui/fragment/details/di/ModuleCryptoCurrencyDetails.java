package com.povio.cryptoprices.ui.fragment.details.di;

import com.povio.cryptoprices.annotation.PerActivity;
import com.povio.cryptoprices.errorhandler.ErrorHandler;
import com.povio.cryptoprices.network.NetworkManager;
import com.povio.cryptoprices.preference.CryptoPreference;
import com.povio.cryptoprices.ui.fragment.details.CryptoCurrencyDetailsInteractor;
import com.povio.cryptoprices.ui.fragment.details.CryptoCurrencyDetailsInteractorImpl;
import com.povio.cryptoprices.ui.fragment.details.CryptoCurrencyDetailsPresenter;
import com.povio.cryptoprices.ui.fragment.details.CryptoCurrencyDetailsPresenterImpl;
import com.povio.cryptoprices.ui.fragment.details.CryptoCurrencyDetailsView;
import com.povio.cryptoprices.ui.fragment.list.api.APICryptoData;
import com.povio.cryptoprices.ui.fragment.list.dto.CryptoElement;
import com.povio.cryptoprices.util.ResourceGetter;
import com.povio.cryptoprices.util.rxutils.MSchedulers;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

import static com.povio.cryptoprices.network.http.Constants.RETROFIT_API;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/18/18.
 */
@Module
public class ModuleCryptoCurrencyDetails {

    private final CryptoCurrencyDetailsView view;
    private final CryptoElement cryptoCurrencyDetails;

    public ModuleCryptoCurrencyDetails(CryptoCurrencyDetailsView view, CryptoElement cryptoCurrencyDetails) {
        this.view = view;
        this.cryptoCurrencyDetails = cryptoCurrencyDetails;
    }

    @PerActivity
    @Provides
    public CryptoCurrencyDetailsPresenter providePresenter(CryptoCurrencyDetailsInteractor interactor, NetworkManager networkManager, CryptoPreference preference) {
        return new CryptoCurrencyDetailsPresenterImpl(view, interactor, cryptoCurrencyDetails, networkManager, preference);
    }

    @PerActivity
    @Provides
    public CryptoCurrencyDetailsInteractor provideInteractor(ErrorHandler errorHandler, CryptoPreference preference, APICryptoData api, MSchedulers schedulers, ResourceGetter resourceGetter) {
        return new CryptoCurrencyDetailsInteractorImpl(errorHandler, preference, api, schedulers, resourceGetter);
    }

    /**
     * Provides retrofit API
     */
    @PerActivity
    @Provides
    public APICryptoData provideAPI(@Named(RETROFIT_API) Retrofit retrofit) {
        return retrofit.create(APICryptoData.class);
    }
}
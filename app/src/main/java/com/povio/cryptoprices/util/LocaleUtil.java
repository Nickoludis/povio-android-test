package com.povio.cryptoprices.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

/**
 * Util class which encapsulates static methods for converting date/time and number values into corresponding formats proper for displaying.
 * <p>
 * Created by nbrankovic on 5/12/17.
 */
public class LocaleUtil {

    static final String formatStringWithZeroDecimals = "###,##0.00";
    static final String formatStringWithoutZeroDecimals = "###,##0.##";
    static DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());

    /**
     * Change statically decimal and grouping separator.
     */
    static {
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator(',');
    }

    static DecimalFormat decimalFormatWithZeroDecimals = new DecimalFormat(formatStringWithZeroDecimals, otherSymbols);
    static DecimalFormat decimalFormatWithoutZeroDecimals = new DecimalFormat(formatStringWithoutZeroDecimals, otherSymbols);

    /**
     * Converts number value into formatted string, with two zeros after decimal separator.
     *
     * @param number value to convert
     * @return properly formatted string value
     */
    public static String getStringFormatForNumberWithZeroDecimals(double number) {
        return decimalFormatWithZeroDecimals.format(number);
    }

    /**
     * Converts number value into formatted string, without two zeros after decimal separator.
     *
     * @param number value to convert
     * @return properly formatted string value
     */
    public static String getStringFormatForNumberWithoutZeroDecimals(double number) {
        return decimalFormatWithoutZeroDecimals.format(number);
    }

    public static Double getDoubleFromFormattedNumberWithZeroDecimals(String numberInString){
        try {
            return decimalFormatWithoutZeroDecimals.parse(numberInString).doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }
}

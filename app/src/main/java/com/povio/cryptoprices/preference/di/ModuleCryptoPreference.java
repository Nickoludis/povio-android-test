package com.povio.cryptoprices.preference.di;

import android.app.Application;

import com.povio.cryptoprices.annotation.PerActivity;
import com.povio.cryptoprices.preference.CryptoPreference;
import com.povio.cryptoprices.preference.CryptoPreferenceImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/17/18.
 */
@Module
public class ModuleCryptoPreference {

    private final Application app;

    public ModuleCryptoPreference(Application app) {
        this.app = app;
    }

    @PerActivity
    @Provides
    public CryptoPreference provideCryptoPreference() {
        return new CryptoPreferenceImpl(app);
    }
}
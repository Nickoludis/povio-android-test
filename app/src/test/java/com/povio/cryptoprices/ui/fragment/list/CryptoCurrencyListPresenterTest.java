package com.povio.cryptoprices.ui.fragment.list;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.povio.cryptoprices.errorhandler.ErrorHandler;
import com.povio.cryptoprices.network.NNetworkInfo;
import com.povio.cryptoprices.network.NetworkManager;
import com.povio.cryptoprices.network.NetworkStateReceiverListener;
import com.povio.cryptoprices.preference.CryptoPreference;
import com.povio.cryptoprices.ui.fragment.list.api.APICryptoData;
import com.povio.cryptoprices.ui.fragment.list.dto.DTOCryptoElement;
import com.povio.cryptoprices.ui.fragment.list.utils.FileUtils;
import com.povio.cryptoprices.util.ResourceGetter;
import com.povio.cryptoprices.util.rxutils.MSchedulers;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;

import rx.Observable;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CryptoCurrencyListPresenterTest {

    private static final String FILE_NAME_MOCKED_JSON = "MockedData.json";

    ObjectMapper mapper = new ObjectMapper();

    private CryptoCurrencyListView view;
    private CryptoCurrencyListInteractor interactor;
    private CryptoCurrencyListPresenterImpl presenter;
    private ErrorHandler errorHandler;
    private APICryptoData api;
    private MSchedulers schedulers;
    private NetworkManager networkManager;
    private ResourceGetter resourceGetter;
    private CryptoPreference cryptoPreference;

    private String fiatCurrency = "EUR";
    private String limit = "10";

    /**
     * Do all mocks before tests.
     */
    @Before
    public void setUp() throws Exception {

        view = mock(CryptoCurrencyListView.class);
        errorHandler = mock(ErrorHandler.class);
        api = mock(APICryptoData.class);
        schedulers = mock(MSchedulers.class);
        networkManager = mock(NetworkManager.class);
        resourceGetter = mock(ResourceGetter.class);
        cryptoPreference = mock(CryptoPreference.class);

        interactor = new CryptoCurrencyListInteractorImpl(errorHandler, api, schedulers, resourceGetter, cryptoPreference);

        presenter = new CryptoCurrencyListPresenterImpl(view, interactor, networkManager, cryptoPreference);

        when(api.getCryptoList(any(), any())).thenReturn(getMockedJsonList());
    }

    @After
    public void tearDown() throws Exception {

    }

    /**
     * Immediately after the fragment is being loaded {@link CryptoCurrencyListPresenter#loadData()} )} method should be invoked.
     */
    @Test
    public void onFragmentStartLoadJsonData() throws Exception {

        presenter.loadData();

        verifyLoadingActions();

//        simulateDataLoadSuccessfully();
//        verify(view, times(1)).showLoadingAnimation(false);
//        verify(view, times(1)).setData(anyList());
    }

    /**
     * Check is fragment un-subscribed from {@link NetworkManager} after {@link CryptoCurrencyListPresenter#onDestroy()} method is being called.
     */
    @Test
    public void onDestroyRemoveNetworkManagerListener() throws Exception {

        presenter.onDestroy();

        verify(networkManager, times(1)).removeListener(any(NetworkStateReceiverListener.class));
    }

    /**
     * After internet is back check if load data routine is re-engaged.
     */
    @Test
    public void onConnectionEstablishLoadJsonData() throws Exception {

        NNetworkInfo info = new NNetworkInfo(NNetworkInfo.State.CONNECTED);

        presenter.onNetworkChange(info);

        verify(view, times(1)).showLoadingAnimation(false);

    }

    //************ HELPER METHODS ************//

    /**
     * It should simulate engaging of {@link com.povio.cryptoprices.util.rxutils.PresenterCallbackOnResult#onResult(Object)} ()}
     */
    private void simulateDataLoadSuccessfully() {
        doAnswer(invocation -> {
            CryptoCurrencyListInteractor.CryptoListListener listener = (CryptoCurrencyListInteractor.CryptoListListener) invocation.getArguments()[0];
            listener.onResult(anyList());
            return null;
        }).when(interactor).getData(any(CryptoCurrencyListInteractor.CryptoListListener.class), any(), any());
    }

    /**
     * Checks and verifies if fragment is registered to listen for network changes and is loading animation activated.
     */
    private void verifyLoadingActions() {
        presenter.networkStatusListenerSet = false;
        verify(networkManager).addListener(any(NetworkStateReceiverListener.class));
        verify(view, times(1)).showLoadingAnimation(true);
    }

    /**
     * Returns Observable with pre-cached json data held locally.
     */
    private Observable<List<DTOCryptoElement>> getMockedJsonList() {
        List<DTOCryptoElement> list = null;
        try {
            list = mapper.readValue(FileUtils.getFileResource(FILE_NAME_MOCKED_JSON),
                    new TypeReference<List<DTOCryptoElement>>() {
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Observable.just(list);
    }
}
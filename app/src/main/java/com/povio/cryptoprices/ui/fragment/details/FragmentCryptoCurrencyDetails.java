package com.povio.cryptoprices.ui.fragment.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.povio.cryptoprices.R;
import com.povio.cryptoprices.util.InjectorHelper;
import com.povio.cryptoprices.util.Logger;
import com.povio.cryptoprices.util.UiUtil;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/18/18.
 */
public class FragmentCryptoCurrencyDetails extends Fragment implements CryptoCurrencyDetailsView, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.swipe_container_details)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.tv_symbol)
    TextView tv_symbol;

    @BindView(R.id.tv_rank)
    TextView tv_rank;

    @BindView(R.id.tv_name)
    TextView tv_name;

    @BindView(R.id.tv_price)
    TextView tv_price;

    @BindView(R.id.tv_24h_volume)
    TextView tv_24h_volume;

    @BindView(R.id.tv_market_cap)
    TextView tv_market_cap;

    @BindView(R.id.tv_bitcoin_price)
    TextView tv_bitcoin_price;

    @BindView(R.id.tv_1h_change)
    TextView tv_1h_change;

    @BindView(R.id.tv_24h_change)
    TextView tv_24h_change;

    @BindView(R.id.tv_7d_change)
    TextView tv_7d_change;

    @BindView(R.id.tv_total_supply)
    TextView tv_total_supply;

    @BindView(R.id.tv_available_supply)
    TextView tv_available_supply;

    @Inject
    CryptoCurrencyDetailsPresenter presenter;

    private Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InjectorHelper.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_crypto_details_layout, container, false);
        unbinder = ButterKnife.bind(this, view);

        setHasOptionsMenu(true);

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        Logger.d(FragmentCryptoCurrencyDetails.class, "onCreateView: " + this);

        presenter.setData();

        return view;
    }

    @Override
    public void onRefresh() {
        presenter.refreshData();
    }

    @Override
    public void setRank(String rank) {
        tv_rank.setText(rank);
    }

    @Override
    public void setName(String name) {
        tv_name.setText(name);
    }

    @Override
    public void setSymbol(String symbol) {
        tv_symbol.setText(symbol);
    }

    @Override
    public void setPrice(String price) {
        tv_price.setText(price);
    }

    @Override
    public void set24hVolume(String volume24h) {
        tv_24h_volume.setText(volume24h);
    }

    @Override
    public void setMarketCap(String marketCap) {
        tv_market_cap.setText(marketCap);
    }

    @Override
    public void setPriceInBitcoin(String priceInBitcoin) {
        tv_bitcoin_price.setText(priceInBitcoin);
    }

    @Override
    public void setChange1h(String change1h, int color) {
        tv_1h_change.setText(change1h);
        UiUtil.setTextColor(tv_1h_change, color, getActivity());
    }

    @Override
    public void setChange24h(String change24h, int color) {
        tv_24h_change.setText(change24h);
        UiUtil.setTextColor(tv_24h_change, color, getActivity());
    }

    @Override
    public void setChange7d(String change7d, int color) {
        tv_7d_change.setText(change7d);
        UiUtil.setTextColor(tv_7d_change, color, getActivity());
    }

    @Override
    public void setTotalSupply(String totalSupply) {
        tv_total_supply.setText(totalSupply);
    }

    @Override
    public void setAvailableSupply(String availableSupply) {
        tv_available_supply.setText(availableSupply);
    }

    @Override
    public void showLoadingAnimation(boolean show) {
        mSwipeRefreshLayout.setRefreshing(show);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Hide search item from action bar
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onStop() {
        presenter.cancelLoading();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroy();
        super.onDestroyView();
        unbinder.unbind();
        Logger.d(FragmentCryptoCurrencyDetails.class, "onDestroyView: " + this);
    }
}
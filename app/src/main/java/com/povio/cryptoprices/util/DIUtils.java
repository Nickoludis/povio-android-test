package com.povio.cryptoprices.util;

import android.app.Activity;

import com.povio.cryptoprices.ComponentApp;
import com.povio.cryptoprices.ComponentProvider;
import com.povio.cryptoprices.ui.activity.di.ComponentActivity;

public class DIUtils {

    public static ComponentActivity getComponentActivity(android.support.v4.app.Fragment fragment) {
        ComponentProvider componentProvider = (ComponentProvider) fragment.getActivity();
        return componentProvider.component(ComponentActivity.class);
    }

    public static ComponentActivity getComponentActivity(Activity activity) {
        ComponentProvider componentProvider = (ComponentProvider) activity;
        return componentProvider.component(ComponentActivity.class);
    }

    public static ComponentApp getComponentApp(Activity activity) {
        ComponentProvider componentProvider = (ComponentProvider) activity;
        return componentProvider.component(ComponentApp.class);
    }
}

package com.povio.cryptoprices.navigation.action;

public interface ActionBackPress {
    /**
     * Returns false if fragment has handled event, otherwise true.
     */
    boolean onBackPress();
}
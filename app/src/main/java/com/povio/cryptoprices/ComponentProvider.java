package com.povio.cryptoprices;

public interface ComponentProvider {
    <T> T component(Class<T> type);
}
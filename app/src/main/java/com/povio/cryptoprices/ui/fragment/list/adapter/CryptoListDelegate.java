package com.povio.cryptoprices.ui.fragment.list.adapter;


import com.povio.cryptoprices.ui.fragment.list.dto.CryptoElement;

public interface CryptoListDelegate {

    /**
     * Propagates list item click event to the fragment.
     *
     * @param element list element data
     */
    void onItemClick(CryptoElement element);
}

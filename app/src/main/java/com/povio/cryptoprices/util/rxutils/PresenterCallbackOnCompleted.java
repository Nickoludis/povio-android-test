package com.povio.cryptoprices.util.rxutils;

public interface PresenterCallbackOnCompleted {
    void onCompleted();
}

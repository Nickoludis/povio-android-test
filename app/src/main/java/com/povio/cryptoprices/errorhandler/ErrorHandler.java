package com.povio.cryptoprices.errorhandler;

public interface ErrorHandler {

    /**
     * Should check and react on passed error object.
     */
    void error(Throwable error);
}

package com.povio.cryptoprices.network;

public interface NetworkManager {

    void addListener(NetworkStateReceiverListener l);

    void removeListener(NetworkStateReceiverListener l);

    boolean isNetworkAvailable();
}

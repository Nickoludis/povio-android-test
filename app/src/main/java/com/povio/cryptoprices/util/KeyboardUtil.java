package com.povio.cryptoprices.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */

public class KeyboardUtil {

    public static void hideKeyboard(Activity context) {
        if (context != null) {
            View view = context.getCurrentFocus();
            hideKeyboard(context, view);
        }
    }

    public static void hideKeyboard(Activity context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            view.clearFocus();
        } else {
            context.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }

    public static void focusEditViewAndShowKeyboard(Activity context, EditText editTextView){
        if(context==null) return;

        if(editTextView != null && editTextView.requestFocus()) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editTextView, InputMethodManager.SHOW_IMPLICIT);
        }
    }
}

package com.povio.cryptoprices.util;

import android.content.Context;
import android.os.Build;
import android.widget.TextView;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/18/18.
 */

public class UiUtil {

    public static void setTextColor(TextView textView, int color, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setTextColor(context.getColor(color));
        } else {
            textView.setTextColor(context.getResources().getColor(color));
        }
    }
}

package com.povio.cryptoprices.navigation.di;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.povio.cryptoprices.annotation.PerActivity;
import com.povio.cryptoprices.navigation.FragmentTransactionExecutorImpl;
import com.povio.cryptoprices.navigation.NavigationController;
import com.povio.cryptoprices.navigation.NavigationControllerImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ModuleNavigationController {

    @IdRes
    private final int layoutId;
    @NonNull
    private final FragmentManager fragmentManager;

    public ModuleNavigationController(@IdRes int layoutId, @NonNull FragmentManager fragmentManager) {
        this.layoutId = layoutId;
        this.fragmentManager = fragmentManager;
    }

    @PerActivity
    @Provides
    public NavigationController provideNavigationController() {
        return new NavigationControllerImpl(new FragmentTransactionExecutorImpl(layoutId, fragmentManager));
    }
}

package com.povio.cryptoprices.navigation.action;


import android.support.annotation.NonNull;

import com.povio.cryptoprices.animation.AnimationUtils;
import com.povio.cryptoprices.navigation.NavigationActionBuilder;

/**
 * Represents parameter in fragment transaction operation.
 */
public interface NavigationActionAnimationType {

    /**
     * Set animation type to be executed during fragment transaction.
     *
     * @param animationType type of animation
     * @return NavigationActionBuilder instance
     */
    NavigationActionBuilder animation(@NonNull AnimationUtils.Transition animationType);
}

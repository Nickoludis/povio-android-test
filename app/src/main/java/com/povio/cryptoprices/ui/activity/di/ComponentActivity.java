package com.povio.cryptoprices.ui.activity.di;

import com.povio.cryptoprices.annotation.PerActivity;
import com.povio.cryptoprices.dialog.di.ModuleDialogManager;
import com.povio.cryptoprices.errorhandler.di.ModuleErrorHandler;
import com.povio.cryptoprices.navigation.di.ModuleNavigationController;
import com.povio.cryptoprices.preference.di.ModuleCryptoPreference;
import com.povio.cryptoprices.ui.fragment.details.di.ComponentCryptoCurrencyDetails;
import com.povio.cryptoprices.ui.fragment.details.di.ModuleCryptoCurrencyDetails;
import com.povio.cryptoprices.ui.fragment.list.di.ComponentCryptoCurrencyList;
import com.povio.cryptoprices.ui.fragment.list.di.ModuleCryptoCurrencyList;
import com.povio.cryptoprices.ui.fragment.settings.ComponentCryptoSettings;
import com.povio.cryptoprices.ui.fragment.settings.ModuleCryptoSettings;

import dagger.Subcomponent;


@PerActivity
@Subcomponent(modules = {ModuleErrorHandler.class, ModuleNavigationController.class, ModuleDialogManager.class, ModuleCryptoPreference.class})
public interface ComponentActivity {

    ComponentCryptoListActivity get();

    ComponentCryptoCurrencyList get(ModuleCryptoCurrencyList module);

    ComponentCryptoSettings get(ModuleCryptoSettings module);

    ComponentCryptoCurrencyDetails get(ModuleCryptoCurrencyDetails module);
}

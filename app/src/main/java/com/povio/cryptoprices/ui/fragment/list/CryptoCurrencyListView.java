package com.povio.cryptoprices.ui.fragment.list;

import com.povio.cryptoprices.ui.fragment.list.dto.CryptoElement;

import java.util.List;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */
public interface CryptoCurrencyListView {

    /**
     * Provides loaded data to the view
     *
     * @param result
     */
    void setData(List<CryptoElement> result);

    /**
     * Show / hide loading progress animation
     */
    void showLoadingAnimation(boolean show);
}
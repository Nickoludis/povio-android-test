package com.povio.cryptoprices.ui.fragment.settings;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.povio.cryptoprices.R;
import com.povio.cryptoprices.annotation.PerActivity;
import com.povio.cryptoprices.preference.CryptoPreference;
import com.povio.cryptoprices.util.ResourceGetter;

import java.util.Arrays;
import java.util.List;

import dagger.Module;
import dagger.Provides;

import static com.povio.cryptoprices.preference.Constantns.CURRENCY_CNY;
import static com.povio.cryptoprices.preference.Constantns.CURRENCY_EUR;
import static com.povio.cryptoprices.preference.Constantns.CURRENCY_USD;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */
@Module
public class ModuleCryptoSettings {

    private final CryptoSettingsView view;
    private final Context context;

    public ModuleCryptoSettings(CryptoSettingsView view, Context context) {
        this.view = view;
        this.context = context;
    }

    @PerActivity
    @Provides
    public CryptoSettingsPresenter providePresenter(CryptoPreference cryptoPreference) {
        return new CryptoSettingsPresenterImpl(view, cryptoPreference);
    }

    @PerActivity
    @Provides
    public ArrayAdapter provideCurrencyArrayAdapter(ResourceGetter resourceGetter) {
        List<SpinnerValue> values = Arrays.asList(
                new SpinnerValue(CURRENCY_EUR, resourceGetter.getString(R.string.spinner_eur_name)),
                new SpinnerValue(CURRENCY_USD, resourceGetter.getString(R.string.spinner_usd_name)),
                new SpinnerValue(CURRENCY_CNY, resourceGetter.getString(R.string.spinner_cny_name)));
        return new ArrayAdapter(context, R.layout.spinner_currency_item_layout, values);
    }
}
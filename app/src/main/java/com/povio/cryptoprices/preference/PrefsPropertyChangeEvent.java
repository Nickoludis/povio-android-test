package com.povio.cryptoprices.preference;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/17/18.
 */

public class PrefsPropertyChangeEvent {

    /**
     * Constructs a new <code>PrefsPropertyChangeEvent</code>.
     *
     * @param propertyName The programmatic name of the property
     *                     that was changed.
     * @param oldValue     The old value of the property.
     * @param newValue     The new value of the property.
     */
    public PrefsPropertyChangeEvent(Object source, String propertyName,
                                    Object oldValue, Object newValue) {
        this.propertyName = propertyName;
        this.newValue = newValue;
        this.oldValue = oldValue;
    }

    /**
     * Gets the programmatic name of the property that was changed.
     *
     * @return The programmatic name of the property that was changed.
     * May be null if multiple properties have changed.
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * Gets the new value for the property, expressed as an Object.
     *
     * @return The new value for the property, expressed as an Object.
     * May be null if multiple properties have changed.
     */
    public Object getNewValue() {
        return newValue;
    }

    /**
     * Gets the old value for the property, expressed as an Object.
     *
     * @return The old value for the property, expressed as an Object.
     * May be null if multiple properties have changed.
     */
    public Object getOldValue() {
        return oldValue;
    }


    /**
     * name of the property that changed.  May be null, if not known.
     *
     * @serial
     */
    private String propertyName;

    /**
     * New value for property.  May be null if not known.
     *
     * @serial
     */
    private Object newValue;

    /**
     * Previous value for property.  May be null if not known.
     *
     * @serial
     */
    private Object oldValue;

}


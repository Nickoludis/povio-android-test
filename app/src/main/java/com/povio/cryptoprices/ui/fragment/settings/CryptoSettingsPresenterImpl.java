package com.povio.cryptoprices.ui.fragment.settings;

import com.povio.cryptoprices.preference.CryptoPreference;

import static com.povio.cryptoprices.preference.Constantns.CURRENCY_NUMBER_LIMIT_MAX;
import static com.povio.cryptoprices.preference.Constantns.CURRENCY_NUMBER_LIMIT_MIN;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */
public class CryptoSettingsPresenterImpl implements CryptoSettingsPresenter {

    private final CryptoSettingsView view;
    private final CryptoPreference cryptoPreference;

    private String currentFiatCurrency;
    private int currentNumberLimit;

    public CryptoSettingsPresenterImpl(CryptoSettingsView view, CryptoPreference cryptoPreference) {
        this.view = view;
        this.cryptoPreference = cryptoPreference;
    }

    @Override
    public void setView() {
        view.initCurrencyNumberLimit(CURRENCY_NUMBER_LIMIT_MIN, CURRENCY_NUMBER_LIMIT_MAX);
        currentFiatCurrency = cryptoPreference.getFiatCurrency();
        view.setCurrency(currentFiatCurrency);
        currentNumberLimit = cryptoPreference.getCurrencyNumberLimit();
        view.setCurrencyLimit(currentNumberLimit);
    }

    @Override
    public void save() {
        boolean notify = false;
        if (!currentFiatCurrency.equals(view.getCurrency())) {
            cryptoPreference.setFiatCurrency(view.getCurrency(), false);
            notify = true;
        }
        if (currentNumberLimit != view.getCurrencyLimit()) {
            cryptoPreference.setCurrencyNumberLimit(view.getCurrencyLimit(), false);
            notify = true;
        }
        if (notify) cryptoPreference.notifyPrefsListener();
        view.close();
    }
}
package com.povio.cryptoprices.errorhandler.di;

import android.app.Activity;

import com.povio.cryptoprices.annotation.PerActivity;
import com.povio.cryptoprices.dialog.DialogManager;
import com.povio.cryptoprices.errorhandler.ErrorHandler;
import com.povio.cryptoprices.errorhandler.ErrorHandlerImp;
import com.povio.cryptoprices.network.NetworkManager;

import dagger.Module;
import dagger.Provides;

@Module
public class ModuleErrorHandler {

    private final Activity context;

    public ModuleErrorHandler(Activity context) {
        this.context = context;
    }

    @PerActivity
    @Provides
    public ErrorHandler provideErrorHandler(NetworkManager networkManager, DialogManager dialogManager) {
        return new ErrorHandlerImp(context, networkManager, dialogManager);
    }
}
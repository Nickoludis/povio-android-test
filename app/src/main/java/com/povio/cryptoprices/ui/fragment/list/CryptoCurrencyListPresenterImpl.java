package com.povio.cryptoprices.ui.fragment.list;

import com.povio.cryptoprices.network.NNetworkInfo;
import com.povio.cryptoprices.network.NetworkManager;
import com.povio.cryptoprices.network.NetworkStateReceiverListener;
import com.povio.cryptoprices.preference.CryptoPreference;
import com.povio.cryptoprices.preference.PrefsPropertyChangeEvent;
import com.povio.cryptoprices.preference.PrefsPropertyListener;
import com.povio.cryptoprices.ui.fragment.list.dto.CryptoElement;
import com.povio.cryptoprices.util.rxutils.Task;

import java.util.List;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */
public class CryptoCurrencyListPresenterImpl implements CryptoCurrencyListPresenter, NetworkStateReceiverListener, PrefsPropertyListener {

    private final CryptoCurrencyListView view;
    private CryptoCurrencyListInteractor interactor;
    private final NetworkManager networkManager;
    private final CryptoPreference preference;

    private Task loadTask;
    protected boolean networkStatusListenerSet = false;

    public CryptoCurrencyListPresenterImpl(CryptoCurrencyListView view, CryptoCurrencyListInteractor interactor, NetworkManager networkManager, CryptoPreference preference) {
        this.view = view;
        this.interactor = interactor;
        this.networkManager = networkManager;
        this.preference = preference;
    }

    @Override
    public void loadData() {
        if (!networkStatusListenerSet) {
            networkManager.addListener(this);
            networkStatusListenerSet = true;
        }
        preference.addPrefsPropertyListener(this);
        view.showLoadingAnimation(true);
        executeLoadJob();
    }

    @Override
    public void cancelLoading() {
        if (loadTask != null) loadTask.end();
    }

    @Override
    public void onNetworkChange(NNetworkInfo info) {
        if (info.isConnected()) {
            executeLoadJob();
        }
    }

    @Override
    public void onDestroy() {
        networkManager.removeListener(this);
        preference.removePrefsPropertyListener(this);
    }

    @Override
    public void onNetworkStateChange(NNetworkInfo info) {
        onNetworkChange(info);
    }

    @Override
    public void propertyChange(PrefsPropertyChangeEvent evt) {
        String propertyName = evt.getPropertyName();
        if (preference.PROPERTY_ANY.equals(propertyName)) {
            view.showLoadingAnimation(true);
            executeLoadJob();
        }
    }

    /////////////////////
    // PRIVATE METHODS //
    /////////////////////

    private void executeLoadJob() {

        if (loadTask == null || loadTask.isEnded()) {

            loadTask = interactor.getData(new CryptoCurrencyListInteractor.CryptoListListener() {
                @Override
                public boolean onError(Throwable error) {
                    view.showLoadingAnimation(false);
                    return false;
                }

                @Override
                public void onCompleted() {
                    view.showLoadingAnimation(false);
                }

                @Override
                public void onResult(List<CryptoElement> result) {
                    view.setData(result);
                }
            }, preference.getFiatCurrency(), Integer.toString(preference.getCurrencyNumberLimit()));
        }
    }
}
package com.povio.cryptoprices.ui.fragment.settings;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */
public interface CryptoSettingsView {

    void initCurrencyNumberLimit(int min, int max);

    void setCurrency(String currency);

    String getCurrency();

    void setCurrencyLimit(int limit);

    int getCurrencyLimit();

    void close();
}
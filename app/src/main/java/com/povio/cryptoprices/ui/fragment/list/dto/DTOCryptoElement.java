package com.povio.cryptoprices.ui.fragment.list.dto;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class DTOCryptoElement {
    public final String id;
    public final String name;
    public final String symbol;
    public final String rank;
    public final double price_usd;
    public final double price_btc;
    public final double volume_usd_24h;
    public final double market_cap_usd;
    public final double market_cap_cni;
    public final String available_supply;
    public final String total_supply;
    public final String max_supply;
    public final double percent_change_1h;
    public final double percent_change_24h;
    public final double percent_change_7d;
    public final String last_updated;
    public final double price_eur;
    public final double price_cny;
    public final double volume_eur_24h;
    public final double volume_cni_24h;
    public final double market_cap_eur;

    @JsonCreator
    public DTOCryptoElement(@JsonProperty("id") String id, @JsonProperty("name") String name, @JsonProperty("symbol") String symbol, @JsonProperty("rank") String rank, @JsonProperty("price_usd") double price_usd, @JsonProperty("price_btc") double price_btc, @JsonProperty("24h_volume_usd") double volume_usd_24h, @JsonProperty("market_cap_usd") double market_cap_usd, @JsonProperty("market_cap_cny") double market_cap_cni, @JsonProperty("available_supply") String available_supply, @JsonProperty("total_supply") String total_supply, @JsonProperty("max_supply") String max_supply, @JsonProperty("percent_change_1h") double percent_change_1h, @JsonProperty("percent_change_24h") double percent_change_24h, @JsonProperty("percent_change_7d") double percent_change_7d, @JsonProperty("last_updated") String last_updated, @JsonProperty("price_eur") double price_eur, @JsonProperty("price_cny") double price_cny, @JsonProperty("24h_volume_eur") double volume_eur_24h, @JsonProperty("24h_volume_cny") double volume_cni_24h, @JsonProperty("market_cap_eur") double market_cap_eur) {
        this.id = id;
        this.name = name;
        this.symbol = symbol;
        this.rank = rank;
        this.price_usd = price_usd;
        this.price_btc = price_btc;
        this.volume_usd_24h = volume_usd_24h;
        this.market_cap_usd = market_cap_usd;
        this.market_cap_cni = market_cap_cni;
        this.available_supply = available_supply;
        this.total_supply = total_supply;
        this.max_supply = max_supply;
        this.percent_change_1h = percent_change_1h;
        this.percent_change_24h = percent_change_24h;
        this.percent_change_7d = percent_change_7d;
        this.last_updated = last_updated;
        this.price_eur = price_eur;
        this.price_cny = price_cny;
        this.volume_eur_24h = volume_eur_24h;
        this.volume_cni_24h = volume_cni_24h;
        this.market_cap_eur = market_cap_eur;
    }
}

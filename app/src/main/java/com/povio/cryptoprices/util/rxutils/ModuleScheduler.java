package com.povio.cryptoprices.util.rxutils;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ModuleScheduler {

    @Singleton
    @Provides
    public MSchedulers provideScheduler() {
        return new MSchedulersImp();
    }
}

package com.povio.cryptoprices.dialog.di;

import android.app.Activity;

import com.povio.cryptoprices.annotation.PerActivity;
import com.povio.cryptoprices.dialog.DialogManager;
import com.povio.cryptoprices.dialog.DialogManagerImp;
import com.povio.cryptoprices.navigation.NavigationController;
import com.povio.cryptoprices.util.ResourceGetter;
import com.povio.cryptoprices.util.ResourceGetterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ModuleDialogManager {

    private Activity activity;

    public ModuleDialogManager(Activity activity) {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    public DialogManager provideDialogManager(NavigationController navigationController, ResourceGetter resourceGetter) {
        return new DialogManagerImp(navigationController, resourceGetter);
    }

    @PerActivity
    @Provides
    public ResourceGetter providesResourceGetter() {
        return new ResourceGetterImpl(activity);
    }
}

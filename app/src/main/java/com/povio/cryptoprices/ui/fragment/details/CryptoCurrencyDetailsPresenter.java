package com.povio.cryptoprices.ui.fragment.details;

import com.povio.cryptoprices.network.NNetworkInfo;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/18/18.
 */
public interface CryptoCurrencyDetailsPresenter {

    void setData();

    void refreshData();

    void cancelLoading();

    void onNetworkChange(NNetworkInfo info);

    void onDestroy();
}
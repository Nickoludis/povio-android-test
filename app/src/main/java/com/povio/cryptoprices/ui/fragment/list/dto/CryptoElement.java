package com.povio.cryptoprices.ui.fragment.list.dto;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorRes;

import com.povio.cryptoprices.R;
import com.povio.cryptoprices.util.LocaleUtil;
import com.povio.cryptoprices.util.ResourceGetter;

import static com.povio.cryptoprices.preference.Constantns.CURRENCY_CNY;
import static com.povio.cryptoprices.preference.Constantns.CURRENCY_EUR;
import static com.povio.cryptoprices.preference.Constantns.CURRENCY_USD;

public class CryptoElement implements Parcelable {

    public final String id;

    public final String rank;
    public final String name;
    public final String symbol;

    public String price;
    public String volume24h;
    public String marketCap;

    public final String priceBitCoin;
    public final String totalSupply;
    public final String availableSupply;

    public final String change24h;
    public final String change1h;
    public final String change7d;

    public @ColorRes
    int color24h = R.color.green;
    public @ColorRes
    int color1h = R.color.green;
    public @ColorRes
    int color7d = R.color.green;

    public CryptoElement(DTOCryptoElement dtoCryptoElement, ResourceGetter resourceGetter, String fiatCurrency) {

        this.id = dtoCryptoElement.id;
        this.rank = dtoCryptoElement.rank;
        this.symbol = dtoCryptoElement.symbol;
        this.name = dtoCryptoElement.name;
        this.priceBitCoin = LocaleUtil.getStringFormatForNumberWithZeroDecimals(dtoCryptoElement.price_btc);
        this.totalSupply = dtoCryptoElement.total_supply;
        this.availableSupply = dtoCryptoElement.available_supply;

        if (dtoCryptoElement.percent_change_24h < 0) color24h = R.color.red;
        if (dtoCryptoElement.percent_change_1h < 0) color1h = R.color.red;
        if (dtoCryptoElement.percent_change_7d < 0) color7d = R.color.red;

        this.change24h = dtoCryptoElement.percent_change_24h + resourceGetter.getString(R.string.percentage_sign);
        this.change1h = dtoCryptoElement.percent_change_1h + resourceGetter.getString(R.string.percentage_sign);
        this.change7d = dtoCryptoElement.percent_change_7d + resourceGetter.getString(R.string.percentage_sign);

        if (fiatCurrency.equals(CURRENCY_EUR)) {
            this.price = resourceGetter.getString(R.string.euro_sign) + LocaleUtil.getStringFormatForNumberWithZeroDecimals(dtoCryptoElement.price_eur);
            this.volume24h = resourceGetter.getString(R.string.euro_sign) + LocaleUtil.getStringFormatForNumberWithZeroDecimals(dtoCryptoElement.volume_eur_24h);
            this.marketCap = resourceGetter.getString(R.string.euro_sign) + LocaleUtil.getStringFormatForNumberWithZeroDecimals(dtoCryptoElement.market_cap_eur);
        } else if (fiatCurrency.equals(CURRENCY_USD)) {
            this.price = resourceGetter.getString(R.string.dollar_sign) + LocaleUtil.getStringFormatForNumberWithZeroDecimals(dtoCryptoElement.price_usd);
            this.volume24h = resourceGetter.getString(R.string.dollar_sign) + LocaleUtil.getStringFormatForNumberWithZeroDecimals(dtoCryptoElement.volume_usd_24h);
            this.marketCap = resourceGetter.getString(R.string.dollar_sign) + LocaleUtil.getStringFormatForNumberWithZeroDecimals(dtoCryptoElement.market_cap_usd);
        } else if (fiatCurrency.equals(CURRENCY_CNY)) {
            this.price = LocaleUtil.getStringFormatForNumberWithZeroDecimals(dtoCryptoElement.price_cny) + resourceGetter.getString(R.string.yuan_sign);
            this.volume24h = LocaleUtil.getStringFormatForNumberWithZeroDecimals(dtoCryptoElement.volume_cni_24h) + resourceGetter.getString(R.string.yuan_sign);
            this.marketCap = LocaleUtil.getStringFormatForNumberWithZeroDecimals(dtoCryptoElement.market_cap_cni) + resourceGetter.getString(R.string.yuan_sign);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.rank);
        dest.writeString(this.name);
        dest.writeString(this.symbol);
        dest.writeString(this.price);
        dest.writeString(this.volume24h);
        dest.writeString(this.marketCap);
        dest.writeString(this.priceBitCoin);
        dest.writeString(this.totalSupply);
        dest.writeString(this.availableSupply);
        dest.writeString(this.change24h);
        dest.writeString(this.change1h);
        dest.writeString(this.change7d);
        dest.writeInt(this.color24h);
        dest.writeInt(this.color1h);
        dest.writeInt(this.color7d);
    }

    protected CryptoElement(Parcel in) {
        this.id = in.readString();
        this.rank = in.readString();
        this.name = in.readString();
        this.symbol = in.readString();
        this.price = in.readString();
        this.volume24h = in.readString();
        this.marketCap = in.readString();
        this.priceBitCoin = in.readString();
        this.totalSupply = in.readString();
        this.availableSupply = in.readString();
        this.change24h = in.readString();
        this.change1h = in.readString();
        this.change7d = in.readString();
        this.color24h = in.readInt();
        this.color1h = in.readInt();
        this.color7d = in.readInt();
    }

    public static final Parcelable.Creator<CryptoElement> CREATOR = new Parcelable.Creator<CryptoElement>() {
        @Override
        public CryptoElement createFromParcel(Parcel source) {
            return new CryptoElement(source);
        }

        @Override
        public CryptoElement[] newArray(int size) {
            return new CryptoElement[size];
        }
    };
}

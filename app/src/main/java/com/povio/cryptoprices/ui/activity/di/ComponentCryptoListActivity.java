package com.povio.cryptoprices.ui.activity.di;

import com.povio.cryptoprices.annotation.PerActivity;
import com.povio.cryptoprices.dialog.di.ModuleDialogManager;
import com.povio.cryptoprices.navigation.di.ModuleNavigationController;
import com.povio.cryptoprices.ui.activity.CryptoListActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = {ModuleNavigationController.class, ModuleDialogManager.class})
public interface ComponentCryptoListActivity {

    void inject(CryptoListActivity cryptoListActivity);
}

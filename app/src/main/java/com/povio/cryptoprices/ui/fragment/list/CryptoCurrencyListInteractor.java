package com.povio.cryptoprices.ui.fragment.list;

import com.povio.cryptoprices.ui.fragment.list.dto.CryptoElement;
import com.povio.cryptoprices.util.rxutils.PresenterCallbackErrorHandler;
import com.povio.cryptoprices.util.rxutils.PresenterCallbackOnCompleted;
import com.povio.cryptoprices.util.rxutils.PresenterCallbackOnResult;
import com.povio.cryptoprices.util.rxutils.Task;

import java.util.List;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */
public interface CryptoCurrencyListInteractor {

    interface CryptoListListener extends
            PresenterCallbackOnCompleted,
            PresenterCallbackOnResult<List<CryptoElement>>,
            PresenterCallbackErrorHandler {
    }

    /**
     * @param listener     delegates result
     * @param fiatCurrency "old school" currency to convert crypto currency value into.
     * @param limit        number of top crypto currencies to load
     * @return Instance of {@link Task}
     */
    Task getData(CryptoListListener listener, String fiatCurrency, String limit);
}
package com.povio.cryptoprices.util.rxutils;

public interface PresenterCallbackOnResult<T>  {
    void onResult(T result);
}

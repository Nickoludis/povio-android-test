package com.povio.cryptoprices.util.rxutils;

public interface PresenterCallbackErrorHandler{
    boolean onError(Throwable error);
}

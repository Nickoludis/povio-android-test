package com.povio.cryptoprices.ui.fragment.list.di;

import com.povio.cryptoprices.annotation.PerActivity;
import com.povio.cryptoprices.errorhandler.ErrorHandler;
import com.povio.cryptoprices.network.NetworkManager;
import com.povio.cryptoprices.preference.CryptoPreference;
import com.povio.cryptoprices.ui.fragment.list.CryptoCurrencyListInteractor;
import com.povio.cryptoprices.ui.fragment.list.CryptoCurrencyListInteractorImpl;
import com.povio.cryptoprices.ui.fragment.list.CryptoCurrencyListPresenter;
import com.povio.cryptoprices.ui.fragment.list.CryptoCurrencyListPresenterImpl;
import com.povio.cryptoprices.ui.fragment.list.CryptoCurrencyListView;
import com.povio.cryptoprices.ui.fragment.list.api.APICryptoData;
import com.povio.cryptoprices.util.ResourceGetter;
import com.povio.cryptoprices.util.rxutils.MSchedulers;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

import static com.povio.cryptoprices.network.http.Constants.RETROFIT_API;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */
@Module
public class ModuleCryptoCurrencyList {

    private final CryptoCurrencyListView view;

    public ModuleCryptoCurrencyList(CryptoCurrencyListView view) {
        this.view = view;
    }

    @PerActivity
    @Provides
    public CryptoCurrencyListPresenter providePresenter(CryptoCurrencyListInteractor interactor, NetworkManager networkManager, CryptoPreference preference) {
        return new CryptoCurrencyListPresenterImpl(view, interactor, networkManager, preference);
    }

    @PerActivity
    @Provides
    public CryptoCurrencyListInteractor provideInteractor(ErrorHandler errorHandler, APICryptoData api, MSchedulers schedulers, ResourceGetter resourceGetter, CryptoPreference preference) {
        return new CryptoCurrencyListInteractorImpl(errorHandler, api, schedulers, resourceGetter, preference);
    }

    /**
     * Provides retrofit API
     */
    @PerActivity
    @Provides
    public APICryptoData provideAPI(@Named(RETROFIT_API) Retrofit retrofit) {
        return retrofit.create(APICryptoData.class);
    }
}
package com.povio.cryptoprices.ui.fragment.details;

import com.povio.cryptoprices.ui.fragment.list.dto.CryptoElement;
import com.povio.cryptoprices.util.rxutils.PresenterCallbackErrorHandler;
import com.povio.cryptoprices.util.rxutils.PresenterCallbackOnCompleted;
import com.povio.cryptoprices.util.rxutils.PresenterCallbackOnResult;
import com.povio.cryptoprices.util.rxutils.Task;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/18/18.
 */
public interface CryptoCurrencyDetailsInteractor {

    interface CryptoDetailsListener extends
            PresenterCallbackOnCompleted,
            PresenterCallbackOnResult<CryptoElement>,
            PresenterCallbackErrorHandler {
    }

    /**
     * @param listener     delegates result
     * @param fiatCurrency fiat currency to convert crypto currency value into.
     * @return Instance of {@link Task}
     */
    Task getData(CryptoDetailsListener listener, String cryptoCurrencyId, String fiatCurrency);
}
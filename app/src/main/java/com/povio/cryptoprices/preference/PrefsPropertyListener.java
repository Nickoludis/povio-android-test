package com.povio.cryptoprices.preference;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/17/18.
 */

public interface PrefsPropertyListener {
    void propertyChange(PrefsPropertyChangeEvent evt);
}

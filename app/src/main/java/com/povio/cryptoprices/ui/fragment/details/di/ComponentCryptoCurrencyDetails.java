package com.povio.cryptoprices.ui.fragment.details.di;

import com.povio.cryptoprices.annotation.PerActivity;
import com.povio.cryptoprices.ui.fragment.details.FragmentCryptoCurrencyDetails;

import dagger.Subcomponent;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/18/18.
 */
@PerActivity
@Subcomponent(modules = {ModuleCryptoCurrencyDetails.class})
public interface ComponentCryptoCurrencyDetails {
    void inject(FragmentCryptoCurrencyDetails fragment);
}
package com.povio.cryptoprices.util;

import android.content.Context;
import android.support.annotation.StringRes;


public class ResourceGetterImpl implements ResourceGetter {

    private final Context context;

    public ResourceGetterImpl(Context context) {
        this.context = context;
    }

    public String getString(@StringRes int resource) {
        return context.getResources().getString(resource);
    }

    @Override
    public String getString(@StringRes int resource, @StringRes int resource1) {
        return context.getResources().getString(resource, context.getResources().getString(resource1));
    }

    @Override
    public String getString(@StringRes int resource, String... string) {
        return context.getResources().getString(resource, string);
    }
}

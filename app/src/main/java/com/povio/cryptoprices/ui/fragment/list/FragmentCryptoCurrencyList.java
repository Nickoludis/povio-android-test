package com.povio.cryptoprices.ui.fragment.list;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.povio.cryptoprices.R;
import com.povio.cryptoprices.animation.AnimationUtils;
import com.povio.cryptoprices.navigation.NavigationController;
import com.povio.cryptoprices.navigation.action.ActionBackPress;
import com.povio.cryptoprices.ui.fragment.details.FragmentCryptoCurrencyDetails;
import com.povio.cryptoprices.ui.fragment.list.adapter.CryptoListAdapter;
import com.povio.cryptoprices.ui.fragment.list.adapter.CryptoListDelegate;
import com.povio.cryptoprices.ui.fragment.list.dto.CryptoElement;
import com.povio.cryptoprices.util.InjectorHelper;
import com.povio.cryptoprices.util.KeyboardUtil;
import com.povio.cryptoprices.util.Logger;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.povio.cryptoprices.ui.fragment.details.Constants.DETAILS_BUNDLE_PARAM;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */
public class FragmentCryptoCurrencyList extends Fragment implements CryptoCurrencyListView, CryptoListDelegate, SwipeRefreshLayout.OnRefreshListener, ActionBackPress {

    @BindView(R.id.rv_currency_list)
    RecyclerView recyclerView;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private Unbinder unbinder;

    @Inject
    CryptoCurrencyListPresenter presenter;

    @Inject
    NavigationController navigationController;

    private CryptoListAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InjectorHelper.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_crypto_currency_list_layout, container, false);
        unbinder = ButterKnife.bind(this, view);

        setHasOptionsMenu(true);

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        Logger.d(FragmentCryptoCurrencyList.class, "onCreateView: " + this);

        initAdapter();

        presenter.loadData();

        return view;
    }

    @Override
    public void onStop() {
        presenter.cancelLoading();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroy();
        super.onDestroyView();
        unbinder.unbind();
        Logger.d(FragmentCryptoCurrencyList.class, "onDestroyView: " + this);
    }

    private void initAdapter() {
        if (adapter == null) {
            adapter = new CryptoListAdapter();
            adapter.setListDelegate(this);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        }
    }

    @Override
    public void setData(List<CryptoElement> result) {
        if (result == null) return;
        initAdapter();
        adapter.setData(result);
    }

    @Override
    public void onItemClick(CryptoElement element) {

        KeyboardUtil.hideKeyboard(getActivity(), recyclerView);

        Bundle arguments = new Bundle();
        arguments.putParcelable(DETAILS_BUNDLE_PARAM, element);
        navigationController.loadPage(FragmentCryptoCurrencyDetails.class).
                addToBackStack(true).
                isDialog(false).
                animation(AnimationUtils.Transition.RIGHT_TO_LEFT).
                arguments(arguments).load();
    }

    @Override
    public void showLoadingAnimation(boolean show) {
        mSwipeRefreshLayout.setRefreshing(show);
    }

    SearchView searchView;

    @Override
    public void onRefresh() {
        searchView.setIconified(true);
        searchView.setIconified(true);
        presenter.loadData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_fragment, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                adapter.getFilter().filter(query);
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onBackPress() {
        //close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return false;
        }
        return true;
    }
}
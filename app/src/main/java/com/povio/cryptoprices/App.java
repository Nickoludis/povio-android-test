package com.povio.cryptoprices;

import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import com.povio.cryptoprices.network.NetworkStateReceiver;
import com.povio.cryptoprices.network.di.ModuleNetworkManager;
import com.povio.cryptoprices.network.http.ModuleHTTPClient;
import com.povio.cryptoprices.preference.di.ModuleCryptoPreference;
import com.povio.cryptoprices.util.rxutils.ModuleScheduler;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/15/18.
 */

public class App extends Application implements ComponentProvider {

    private static App app = null;
    private ComponentApp componentApp = null;
    private NetworkStateReceiver networkStateReceiver = new NetworkStateReceiver();

    @Override
    public void onCreate() {
        super.onCreate();

        app = this;

        // Initialize connectivity manager
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkStateReceiver, intentFilter);

        componentApp = DaggerComponentApp.builder()
                .moduleApp(new ModuleApp(this))
                .moduleNetworkManager(new ModuleNetworkManager(networkStateReceiver, connectivityManager))
                .moduleScheduler(new ModuleScheduler())
                .moduleHTTPClient(new ModuleHTTPClient(BuildConfig.BASE_ENDPOINT, "BASE_ENDPOINT"))
                .moduleCryptoPreference(new ModuleCryptoPreference(this))
                .build();
    }

    /**
     * Get application
     *
     * @return application instance
     */
    public static App get() {
        return app;
    }

    @Override
    public <T> T component(Class<T> type) {
        if (ComponentApp.class == type) {
            return (T) componentApp;
        }
        throw new RuntimeException("Unsupported component type :" + type.getSimpleName());
    }
}

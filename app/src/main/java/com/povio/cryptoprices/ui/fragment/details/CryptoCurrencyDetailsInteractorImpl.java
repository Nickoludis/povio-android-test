package com.povio.cryptoprices.ui.fragment.details;

import com.povio.cryptoprices.errorhandler.ErrorHandler;
import com.povio.cryptoprices.preference.CryptoPreference;
import com.povio.cryptoprices.ui.fragment.list.api.APICryptoData;
import com.povio.cryptoprices.ui.fragment.list.dto.CryptoElement;
import com.povio.cryptoprices.util.ResourceGetter;
import com.povio.cryptoprices.util.rxutils.DefaultSubscriber;
import com.povio.cryptoprices.util.rxutils.MSchedulers;
import com.povio.cryptoprices.util.rxutils.Task;
import com.povio.cryptoprices.util.rxutils.TaskImp;

import rx.Observable;
import rx.Subscription;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/18/18.
 */
public class CryptoCurrencyDetailsInteractorImpl implements CryptoCurrencyDetailsInteractor {

    private ErrorHandler errorHandler;
    private final CryptoPreference preference;
    private final APICryptoData api;
    private final MSchedulers schedulers;
    private final ResourceGetter resourceGetter;

    public CryptoCurrencyDetailsInteractorImpl(ErrorHandler errorHandler, CryptoPreference preference, APICryptoData api, MSchedulers schedulers, ResourceGetter resourceGetter) {
        this.errorHandler = errorHandler;
        this.preference = preference;
        this.api = api;
        this.schedulers = schedulers;
        this.resourceGetter = resourceGetter;
    }

    @Override
    public Task getData(CryptoCurrencyDetailsInteractor.CryptoDetailsListener listener, String cryptoCurrencyId, String fiatCurrency) {
        Subscription s = api.getCryptoDetails(cryptoCurrencyId, fiatCurrency)
                .subscribeOn(schedulers.newThread())
                .flatMap(dtoElements -> Observable.from(dtoElements))
                .first()
                .map(dtoElement -> new CryptoElement(dtoElement, resourceGetter, preference.getFiatCurrency()))
                .observeOn(schedulers.mainThread())
                .subscribe(new DefaultSubscriber<>(listener, errorHandler));

        return new TaskImp(s);
    }
}
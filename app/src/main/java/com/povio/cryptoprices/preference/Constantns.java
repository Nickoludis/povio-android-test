package com.povio.cryptoprices.preference;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/18/18.
 */

public class Constantns {

    /**
     * Default values of settings screen properties
     */

    public static final String DEFAULT_FIAT_CURRENCY = "USD";
    public static final int DEFAULT_CURRENCY_NUMBER_LIMIT = 100;

    public static final int CURRENCY_NUMBER_LIMIT_MIN = 0;
    public static final int CURRENCY_NUMBER_LIMIT_MAX = 1600;

    public static final String CURRENCY_EUR = "EUR";
    public static final String CURRENCY_USD = "USD";
    public static final String CURRENCY_CNY = "CNY";
}

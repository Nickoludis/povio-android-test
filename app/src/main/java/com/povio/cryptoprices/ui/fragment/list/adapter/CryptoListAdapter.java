package com.povio.cryptoprices.ui.fragment.list.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;

import com.povio.cryptoprices.R;
import com.povio.cryptoprices.ui.fragment.list.dto.CryptoElement;
import com.povio.cryptoprices.ui.widget.CustomFontTextView;
import com.povio.cryptoprices.util.UiUtil;

import java.util.ArrayList;
import java.util.List;

public class CryptoListAdapter extends RecyclerView.Adapter<CryptoListAdapter.CryptoItemViewHolder> implements Filterable, View.OnClickListener {

    private List<CryptoElement> cryptoList = new ArrayList<>();
    private List<CryptoElement> cryptoListFiltered = new ArrayList<>();
    private CryptoListDelegate cryptoListDelegate;
    private Context context;

    public void setData(@NonNull List<CryptoElement> resultList) {
        this.cryptoList = resultList;
        this.cryptoListFiltered = resultList;
        notifyDataSetChanged();
    }

    /**
     * Sets Adapter's reference to the fragment
     */
    public void setListDelegate(@NonNull CryptoListDelegate cryptoListDelegate) {
        this.cryptoListDelegate = cryptoListDelegate;
    }

    @Override
    public void onBindViewHolder(CryptoItemViewHolder holder, int position) {

        if (cryptoListFiltered == null && cryptoListFiltered.size() == 0) return;

        CryptoElement listElement = cryptoListFiltered.get(position);

        if (listElement == null) return;

        holder.layoutHolder.setTag(R.id.item_details_id, listElement);

        if (!TextUtils.isEmpty(listElement.symbol)) {
            holder.tvSymbol.setText(listElement.symbol);
        } else {
            holder.tvSymbol.setText("");
        }

        if (!TextUtils.isEmpty(listElement.rank)) {
            holder.tvRank.setText(listElement.rank);
        } else {
            holder.tvRank.setText("");
        }

        if (!TextUtils.isEmpty(listElement.price)) {
            holder.tvPrice.setText(listElement.price);
        } else {
            holder.tvPrice.setText("");
        }

        if (!TextUtils.isEmpty(listElement.change24h)) {
            holder.tv24hChange.setText(listElement.change24h);
        } else {
            holder.tv24hChange.setText("");
        }

        UiUtil.setTextColor(holder.tv24hChange, listElement.color24h, context);

        holder.layoutHolder.setOnClickListener(this);
    }

    @Override
    public CryptoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.crypto_list_item_layout, parent, false);
        return new CryptoItemViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        if (cryptoListFiltered != null) {
            return cryptoListFiltered.size();
        }
        return 0;
    }

    @Override
    public void onClick(View view) {
        cryptoListDelegate.onItemClick((CryptoElement) view.getTag(R.id.item_details_id));
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    cryptoListFiltered = cryptoList;

                } else {

                    List<CryptoElement> filteredList = new ArrayList<>();

                    for (CryptoElement row : cryptoList) {
                        if (row.name.toLowerCase().contains(charString.toLowerCase()) || row.symbol.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    cryptoListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = cryptoListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                cryptoListFiltered = (ArrayList<CryptoElement>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class CryptoItemViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout layoutHolder;
        public CustomFontTextView tvSymbol, tvRank, tvPrice, tv24hChange;

        public CryptoItemViewHolder(View view) {
            super(view);
            layoutHolder = view.findViewById(R.id.layout_holder);
            tvSymbol = view.findViewById(R.id.tv_symbol);
            tvRank = view.findViewById(R.id.tv_rank);
            tvPrice = view.findViewById(R.id.tv_price);
            tv24hChange = view.findViewById(R.id.tv_24h_change);
        }
    }
}

package com.povio.cryptoprices.dialog.messaging;

/**
 * Action listener for {@link com.povio.cryptoprices.dialog.Dialog}.
 */
public interface DialogActionListener {
    int ACTION = 0;

    void onClick();
}

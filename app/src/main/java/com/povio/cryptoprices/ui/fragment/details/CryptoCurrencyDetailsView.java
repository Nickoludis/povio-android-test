package com.povio.cryptoprices.ui.fragment.details;

import android.support.annotation.ColorRes;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/18/18.
 */
public interface CryptoCurrencyDetailsView {

    void setRank(String rank);

    void setName(String name);

    void setSymbol(String symbol);

    void setPrice(String price);

    void set24hVolume(String volume24h);

    void setMarketCap(String marketCap);

    void setPriceInBitcoin(String priceInBitcoin);

    void setChange1h(String change1h, @ColorRes int color);

    void setChange24h(String change24h, @ColorRes int color);

    void setChange7d(String change7d, @ColorRes int color);

    void setTotalSupply(String totalSupply);

    void setAvailableSupply(String availableSupply);

    /**
     * Show / hide loading progress animation
     */
    void showLoadingAnimation(boolean show);
}
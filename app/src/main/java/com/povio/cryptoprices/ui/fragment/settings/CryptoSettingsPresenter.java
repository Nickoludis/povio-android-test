package com.povio.cryptoprices.ui.fragment.settings;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */
public interface CryptoSettingsPresenter {

    void setView();

    void save();
}
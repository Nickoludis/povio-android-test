package com.povio.cryptoprices.navigation;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.povio.cryptoprices.animation.AnimationUtils;
import com.povio.cryptoprices.navigation.action.NavigationActionAddToBackStack;
import com.povio.cryptoprices.navigation.action.NavigationActionAnimationType;
import com.povio.cryptoprices.navigation.action.NavigationActionArguments;
import com.povio.cryptoprices.navigation.action.NavigationActionExecute;
import com.povio.cryptoprices.navigation.action.NavigationActionFragmentName;
import com.povio.cryptoprices.navigation.action.NavigationActionIsDialog;

/**
 * Simple builder class which provides fluent way of collecting data for fragment transaction.
 */
public class NavigationActionBuilder implements NavigationActionFragmentName,
        NavigationActionAddToBackStack,
        NavigationActionIsDialog,
        NavigationActionAnimationType,
        NavigationActionArguments,
        NavigationActionExecute {

    private NavigationAction navigationAction = new NavigationAction();
    private NavigationController navigationController;

    public NavigationActionBuilder(NavigationController navigationController) {
        this.navigationController = navigationController;
    }

    @Override
    public NavigationActionAddToBackStack fragment(final Class<? extends Fragment> fragment) {
        navigationAction.fragment = fragment;
        return this;
    }

    @Override
    public NavigationActionIsDialog addToBackStack(boolean addToBackStack) {
        navigationAction.addToBackStack = addToBackStack;
        return this;
    }

    @Override
    public NavigationActionBuilder isDialog(boolean isDialog) {
        navigationAction.isDialog = isDialog;
        return this;
    }

    @Override
    public NavigationActionBuilder animation(final AnimationUtils.Transition animationType) {
        navigationAction.animationType = animationType;
        return this;
    }

    @Override
    public NavigationActionBuilder arguments(Bundle arguments) {
        navigationAction.arguments = arguments;
        return this;
    }

    @Override
    public void load() {
        navigationController.performNavigation(navigationAction);
    }
}

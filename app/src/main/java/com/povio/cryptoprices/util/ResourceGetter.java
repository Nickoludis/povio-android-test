package com.povio.cryptoprices.util;

import android.content.res.Resources;
import android.support.annotation.StringRes;

/**
 * Wrapper around {@link android.content.Context} which exposes {@link Resources} methods.
 */
public interface ResourceGetter {

    String getString(@StringRes int resource);

    String getString(@StringRes int resource, @StringRes int resource1);

    String getString(@StringRes int resource, String... string);
}

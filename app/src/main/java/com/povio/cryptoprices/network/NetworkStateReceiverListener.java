package com.povio.cryptoprices.network;

public interface NetworkStateReceiverListener {
    void onNetworkStateChange(NNetworkInfo info);
}

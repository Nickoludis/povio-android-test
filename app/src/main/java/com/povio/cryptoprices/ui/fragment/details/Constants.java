package com.povio.cryptoprices.ui.fragment.details;

import com.povio.cryptoprices.BuildConfig;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/18/18.
 */

public class Constants {

    public static final String DETAILS_BUNDLE_PARAM = BuildConfig.APPLICATION_ID + ".DETAILS";
}

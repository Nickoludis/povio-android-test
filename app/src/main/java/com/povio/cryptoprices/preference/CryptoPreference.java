package com.povio.cryptoprices.preference;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/17/18.
 */

public interface CryptoPreference {

    String PROPERTY_ANY = "property_any";
    String PROPERTY_FIAT_CURRENCY = "fiat_currency";
    String PROPERTY_CURRENCY_NUMBER_LIMIT = "currency_number_limit";

    String getFiatCurrency();

    void setFiatCurrency(String fiatCurrency, boolean notify);

    int getCurrencyNumberLimit();

    void setCurrencyNumberLimit(int limit, boolean notify);

    void addPrefsPropertyListener(PrefsPropertyListener listener);

    void removePrefsPropertyListener(PrefsPropertyListener listener);

    /**
     * Notify all interested parties that some of the preference properties have changed ( any ).
     */
    void notifyPrefsListener();
}

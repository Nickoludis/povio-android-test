package com.povio.cryptoprices.ui.fragment.list.di;

import com.povio.cryptoprices.annotation.PerActivity;
import com.povio.cryptoprices.ui.fragment.list.FragmentCryptoCurrencyList;

import dagger.Subcomponent;

/**
 * Created by Nikola Brankovic - branick2005@gmail.com on 2/16/18.
 */
@PerActivity
@Subcomponent(modules = {ModuleCryptoCurrencyList.class})
public interface ComponentCryptoCurrencyList {
    void inject(FragmentCryptoCurrencyList fragment);
}
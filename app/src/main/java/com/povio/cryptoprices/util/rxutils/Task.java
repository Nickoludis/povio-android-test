package com.povio.cryptoprices.util.rxutils;

public interface Task {
    void end();
    boolean isEnded();
}
